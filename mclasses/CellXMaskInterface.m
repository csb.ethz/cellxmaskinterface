classdef CellXMaskInterface < handle
    %CellXMaskInterface This class allows for using CellX quantification on
    % pre-segmented mask files
    
    properties
        
        fileSetNum
        configFileName
        configFastERFileName
        config
        fSet
        
        segmImage
        currentSegmentedCells
        currentSegmentationMask
        
        fluoTags
        flatFieldFileNames
        fluoInitialImages
        
        currentAssignment
        trackingIndices
        currentResult
        
        resultFolder
        oofImageFilePath
        
    end
    
    methods
        
        % constructor
        function obj = CellXMaskInterface(fileSetNum, configFileName,...
                segmImage, fluoTags, flatFieldFileNames,...
                fluoInitialImages, resultFolder, ...
                oofImageFilePath)
            
            obj.fileSetNum = fileSetNum;
            obj.configFileName  = configFileName;
            obj.segmImage = segmImage;
            obj.fluoTags = fluoTags;
            obj.flatFieldFileNames = flatFieldFileNames;
            obj.fluoInitialImages = fluoInitialImages;
            obj.resultFolder = resultFolder;
            obj.oofImageFilePath = oofImageFilePath;
            
        end
        
        
        function run(this)
            %----- Mask Integration
            % this.segmImage = imread(SegmentedImagePath);
            
            %---- read the configuration file of the current experiment
            this.config = CellXMaskConfiguration.readXML(this.configFileName);
            this.config.check();
            
            % construct a fileSet
            this.fSet = CellXMaskFileSet(this.fileSetNum, this.oofImageFilePath);
            this.fSet.setResultsDirectory(this.resultFolder);
            
            % check existense of fluoFiles
            if ~isempty(this.fluoTags)
                nrFluoTags = numel(this.fluoTags);
                for nrft = 1:nrFluoTags
                    % pass the parameters of the fluo-images
                    if ~isempty(this.flatFieldFileNames)
                        % take  the flatfield files paths
                        this.fSet.addFluoImageTag('', this.fluoTags{nrft}, this.flatFieldFileNames{nrft} );
                    else
                        this.fSet.addFluoImageTag('', this.fluoTags{nrft});
                    end
                end
            end
            
            % run the segmentation
            if numel(unique(this.segmImage)) == 2
                if all(ismember([0,1], unique(this.segmImage))) == 1 || ...       
                        all(ismember([0,2^8-1], unique(this.segmImage))) == 1 || ...  
                        all(ismember([0,2^16-1], unique(this.segmImage))) == 1 || ...  
                    all(ismember([0,2^32-1], unique(this.segmImage))) == 1        
                    this.segmImage = bwlabel(this.segmImage);                     
                end                                                               
            end                                                                   
            
            cellXSegmenter = CellXMaskSegmenter(this.config, this.segmImage );
            cellXSegmenter.run();
            this.currentSegmentedCells = cellXSegmenter.getDetectedCells();
            fprintf('Detected %d cell(s) on current frame \n', numel(this.currentSegmentedCells));
            
            % run the intensity extractor
            cellXIntensityExtractor = CellXMaskIntensityExtractor(this.config, ...
                this.fSet, this.currentSegmentedCells, this.fluoInitialImages);
            cellXIntensityExtractor.run();
            
            % take the current segmentation mask
            tic
            dim = size(this.segmImage);
            this.currentSegmentationMask = CellXMaskResultExtractor.takeSegmentationMask(...
                this.currentSegmentedCells, dim);
            t=toc;
            fprintf('Extracted current segmentation mask\nElapsed time: %4.2fs', t);
            
            % store current result
            this.currentResult = CellXMaskResultExtractor.extractSegmentationResults(...
                this.fSet, this.currentSegmentedCells, this.config);
                        
            % save mat files
            CellXMaskResultWriter.writeMatSegmentationResults(...
                this.fSet.getSeedsMatFileName(), ...
                this.currentSegmentedCells...
                );
            
            CellXMaskResultWriter.writeSegmentationMask(...
                this.fSet.getMaskMatFileName(), ...
                this.currentSegmentedCells, ...
                cellXSegmenter.getInputImageDimension()...
                );
            
            % write TXT result of the current segmentation
            CellXMaskResultWriter.writeTxtSegmentationResults(...
                this.fSet, ...
                this.currentSegmentedCells, ...
                this.config);
            
            CellXMaskResultWriter.writeSegmentationControlImageWithIndices(...
                this.fSet.oofImage, ...
                this.fSet.controlImageFile, ...
                this.currentSegmentedCells, ...
                this.config...
                );
            CellXMaskResultWriter.writeSeedingControlImage( ...
                this.fSet.oofImage, ...
                this.fSet.seedingImageFile, ...
                cellXSegmenter.seeds, ...
                this.config...
                );
            
            
        end
        
    end
    
end


