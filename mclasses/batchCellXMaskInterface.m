function exitValue = batchCellXMaskInterface(varargin)
%
% This is the deploy function of CellXMaskInterface.
%

% Parse command line arguments
cmdParser = CellXMaskCommandLineParser();
try
    r = cmdParser.parse(varargin);
catch exception
    cmdParser.showUsage();
    exitValue = CellXMaskExceptionHandler.handleException(exception);
    if( isdeployed() )
        exit(exitValue);
    end
    return;
end

try
    if ~any(strcmp(cmdParser.args, '-h'))
        % Read parameter file
        config = CellXMaskConfiguration.readXML(cmdParser.configFile);
        config.check();
    end
catch exception
    exitValue = CellXMaskExceptionHandler.handleException(exception);
    if( isdeployed() )
        exit(exitValue);
    end
    return;
end


try
    if( cmdParser.execMode==1 ) % 
        
        if any(strcmp(cmdParser.args, '-h')) || isempty(cmdParser.args) == 1 || ~all(r == 0)
            cmdParser.showUsage();
            exitValue = 0;
        else
            exitValue = runMode1(cmdParser);
            fprintf('done\n');
        end
        
        
        if( exitValue~=0 )
            if( isdeployed() )
                exit(exitValue);
            end
            return;
        end
        
        
    else % should not be reached
        error('Unknown exec mode');
    end
    
catch exception
    exitValue = CellXMaskExceptionHandler.handleException(exception);
end

if( isdeployed() )
    exit(0);
end
return;

% Process
    function ret = runMode1(cmdParser)
        ret = 1;
        
        fprintf('INFO: Started CellXMask extractor with following parameters:\n')
        % Display input args
        disp(cmdParser)
        % disp(p.Results)
        % Start cellx mask segmenter
        fluoFileArray = {};
        segmentationFileNamesTmp = dir(fullfile(cmdParser.segmentationPath, ...
            cmdParser.segmentationFileNameRegex));
        segmentationResultFiles = natsortfiles(...
            {segmentationFileNamesTmp.name});
        imageFileNamesTmp = dir(fullfile(cmdParser.imagePath, ...
            cmdParser.imageFileNameRegex));
        imageFileArray = natsortfiles({imageFileNamesTmp.name});
        for f = 1:numel(cmdParser.fluoTags)
            fluoTmp = dir(fullfile(cmdParser.imagePath, sprintf('%s*', ...
                cmdParser.fluoTags{f})));
            
            fluoFileArray{f} = natsortfiles({fluoTmp.name});
        end

        frameNumbers = regexpi(imageFileArray,'(?<=_time)(\d*)','match'); % (?<=_time)...
        frameNumbers = str2double([frameNumbers{:}]);
        if frameNumbers(1) == 0
            frameNumbers = frameNumbers + 1;
        end
        % Assumption is that all arrays are of equal size and sorted. I.e. if we
        % have frame number 5 we find the oofImg and fluoImg as the 5th element.
        parfor k = 1:numel(segmentationResultFiles)
            segmImage = fullfile(cmdParser.segmentationPath, ...
                segmentationResultFiles{k});
            oofImage = fullfile(cmdParser.imagePath, imageFileArray{frameNumbers(k)});
            fluoImages = cell(size((cmdParser.fluoTags)));
            if cmdParser.pxPerPlane == 0
                for f = 1:numel(cmdParser.fluoTags)
                    fluoImages{f} = double(imread(fullfile(cmdParser.imagePath, ...
                        fluoFileArray{f}{frameNumbers(k)})));
                end
                segmMask = importdata(segmImage);
                cellxMaskInterface = CellXMaskInterface(frameNumbers(k), ...
                    cmdParser.configFile, segmMask, cmdParser.fluoTags, ...
                    cmdParser.flatFieldFileNames, fluoImages, cmdParser.resultPath, ...
                    oofImage);
                cellxMaskInterface.run()
            else
                for f = 1:numel(cmdParser.fluoTags)
                    fluoImages{f} = fullfile(imagePath, ...
                        fluoFileArray{f}{k});
                end
                u = TracX.Utils();
                io = TracX.IO(u);
                io.maskInterface3D(k, segmImage, oofImage, resultPath, ...
                    cmdParser.pxPerPlane, cmdParser.fluoTags, fluoImages)
            end
        end
        
        ret = 0;
    end

end