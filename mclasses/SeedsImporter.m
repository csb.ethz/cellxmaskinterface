function [ seeds ] = SeedsImporter( this )
%SeedsImporter Imports seeds from segmentation maks
if ~isempty(this.config.cropRegionBoundary)
    p = regionprops(this.currentCropImage, 'all');
else
    p = regionprops(this.image, 'all');
end

% Remove all elements with area == 0
zeroAreaIdx = [p.Area] == 0;
p = p(~zeroAreaIdx);

n = size(p, 1);
seeds =  CellXMaskSeed.empty(n,0);
for i=1:n
    seeds(i) = CellXMaskSeed([], [], []);
    
    seeds(i).houghCenterX      = round(p(i).Centroid(1));
    seeds(i).houghCenterY      = round(p(i).Centroid(2));
    seeds(i).houghRadius = 5;
    seeds(i).id = double(this.image(p(i).PixelIdxList(round(numel(p(i).PixelIdxList)/2))));
    
    
    l =  this.config.maximumCellLength;
    cropRegion = [seeds(i).houghCenterX seeds(i).houghCenterY 2*l 2*l];
    cropRegion(3:4) = cropRegion(3:4)+1;
    seeds(i).setCenterOnCropImage(l,l,cropRegion);
    
    seeds(i).centroid          = p(i).Centroid;
    seeds(i).boundingBox       = p(i).BoundingBox;
    seeds(i).eccentricity      = p(i).Eccentricity;
    seeds(i).orientation       = p(i).Orientation;
    seeds(i).equivDiameter     = p(i).EquivDiameter;
    seeds(i).fracOfGoodMemPixels = nan;
    seeds(i).majorAxisLength    = p(i).MajorAxisLength;
    seeds(i).minorAxisLength    = p(i).MinorAxisLength;
    seeds(i).perimeter          = p(i).Perimeter;
    seeds(i).cellPixelListLindx = p(i).PixelIdxList;
    
    seeds(i).Image = p(i).Image;
    
    %CropPixelIdx = find( p(i).Image > 0);
    %CropPerimeterPixel = find( bwperim( p(i).Image, 8) );
    %CropPerimerterIdx = find(ismember(CropPerimeterPixel,CropPixelIdx));
    [xxx, yyy]= find( bwperim( p(i).Image, 8) );
    if ~isempty(this.config.cropRegionBoundary)
        seeds(i).perimeterPixelListLindx = (size(this.currentCropImage,1)* ...
            ((yyy-1)+p(i).PixelList(1,1)-yyy(1)) + ...
            (xxx+p(i).PixelList(1,2))-xxx(1));
    else
        seeds(i).perimeterPixelListLindx = (size(this.image,1)* ...
            ((yyy-1)+p(i).PixelList(1,1)-yyy(1)) + ...
            (xxx+p(i).PixelList(1,2))-xxx(1));
    end
    % seeds(i).perimeterPixelListLindx = find( bwperim(mask, 8) );
    % seeds(i).perimeterPixelConvolutionValues = this.convolutionImage(seeds(i).perimeterPixelListLindx);
    cytosolMask  = imerode( p(i).Image, this.config.membraneErosionShape);
    seeds(i).cytosolPixelListLindx = find(cytosolMask);
    
    membraneMask = xor( p(i).Image,cytosolMask);
    seeds(i).membranePixelListLindx = find(membraneMask);
    
    halfCellLayerCount = floor( seeds(i).minorAxisLength/2 );
    erosionMask        = p(i).Image; % mask
    erosionShape       = strel('disk', 1);
    membraneShape      = this.config.membraneErosionShape;
    totalVoxelCount    = 0;
    cytosolVoxelCount  = 0;
    cytosolMask        = imerode(erosionMask, membraneShape);
    for j=1:halfCellLayerCount
        if( mod(j,2) )
            erosionMask = imerode(erosionMask, erosionShape);
            cytosolMask = imerode(cytosolMask, erosionShape);
        end
        totalVoxelCount = totalVoxelCount + length(find(erosionMask==1));
        cytosolVoxelCount = cytosolVoxelCount + length(find(cytosolMask==1));
    end
    seeds(i).cellVolume = 2*totalVoxelCount + seeds(i).getCellArea();
    seeds(i).cytosolVolume = 2*cytosolVoxelCount + seeds(i).getCytosolArea();
    
end
end
