classdef CellXMaskSegmenter < handle
    % CELLXMASKSEGMENTER Encapsulates methods and data of image
    % segmentation for the CellXMaskInterface.
    
    properties
        
        % CellXConfiguration instance with the calibration/parameters
        config;
        
        % CellXMembraneDetector instance with maxflow graph for membrane
        % detection
        membraneDetector;
        
        % images
        image;
        imageCLAHE;
        imageExtended;
        imageGradientPhase;
        
        % crop images
        currentCropImage;
        currentCropGradientPhase;
        currentConvolutionImage;
        currentSeed;
        
        % array with CellXMaskSeed handle objects
        seeds;
    end
    
    
    %
    % PUBLIC METHODS
    %
    methods
        
        % constructor
        function obj = CellXMaskSegmenter(config, image)
            obj.config = config;
            obj.image = image;
            if ~isempty(config.cropRegionBoundary)
                obj.currentCropImage = imcrop(obj.image, obj.config.cropRegionBoundary);
            end
        end
        
        function run(this)
            tic;
            fprintf(' Segmenting cells ...\n');
            
            this.loadImages();
            
            try
                this.getSeedsFromSegmentationMask();
            catch exc
                rethrow(exc);
            end
            
            if( this.config.calibrationMode==1 )
                return;
            end
            
            fprintf(' Finished cell segmentation\n');
            t=toc;
            fprintf(' Elapsed time: %4.2fs\n', t);
        end
        
        function vSeeds = getDetectedCells(this)
            vIdx = [this.seeds.skipped]==0;
            vSeeds = this.seeds(vIdx);
        end
        
        
        function ret = getInputImageDimension(this)
            if ~isempty(this.config.cropRegionBoundary)
                ret = size(this.currentCropImage);
            else
                ret = size(this.image);
            end
        end
    end
    
    
    %
    % PRIVATE METHODS
    %
    methods (Access = protected)
        
        function loadImages(this)
            
            if( this.config.calibrationMode==1 )
                return;
            end
            
        end
        
        function initCurrentCrops(this)
            l =  this.config.maximumCellLength;
            cropRegion = [this.currentSeed.houghCenterX this.currentSeed.houghCenterY 2*l 2*l];
            this.currentSeed.setCenterOnCropImage(l,l,cropRegion);
        end
        
        function detectMembranes(this)
            seedCount = numel(this.seeds);
            for i = 1:seedCount
                fprintf('   Processing seed %d\n', i);
                this.currentSeed = this.seeds(i);
                this.initCurrentCrops();
                this.membraneDetector.run(...
                    this.currentSeed, ...
                    this.currentCropImage, ...
                    this.currentCropGradientPhase, ...
                    i);
                if( this.config.debugLevel>1 )
                    disp(this.seeds(i));
                end
            end
        end
        
        function ret = computeBackground(this, image)
            tic;
            fprintf('   Computing segmentation image background \n');
            %range = this.config.intensityClassesCount;
            %if( length(range)>1 )
            %    range = range(1):range(2);
            %end
            %mm = CellXMixtureModel(range);
            %mm.computeMixtureModel(image);
            %[~,idx] = max(mm.getMixingProportions());
            %ret = [mm.getMeanValues(idx) mm.getStdValues(idx)];
            bckVec = image;
            ret = [median(bckVec(:)) std(bckVec(:))];
            t = toc;
            fprintf('   Elapsed time: %4.2fs\n', t);
        end
        
        function getSeedsFromSegmentationMask(this)
            tic;
            fprintf('   Seeds from segmentation mask\n');
            try
                
                if(this.config.isHoughTransformOnCLAHE)
                    this.seeds = SeedsImporter(this);
                    
                else
                    this.seeds = SeedsImporter(this);
                end
                
            catch exc
                rethrow(exc);
            end
            
            fprintf('    Found %d seed(s)\n', numel(this.seeds));
            t=toc;
            fprintf('   Elapsed time: %4.2fs\n', t);
        end
        
    end
end
