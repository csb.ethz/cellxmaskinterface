classdef CellXMaskCommandLineParser < handle
    % CellXMaskCommandLineParser Commandline parser for the
    % CellXMaskInterface.
    
    properties (SetAccess=private)
        
        execMode = 1;
        configFile = [];
                        
        resultPath = [];
        imagePath = [];
        imageFileNameRegex = [];
        segmentationPath = [];
        segmentationFileNameRegex = [];
        pxPerPlane = 0;
        fluoTags = [];
        flatFieldFileNames = [];
        
        seedPrefix;
        maskPrefix;
        
        args;
    end
    
    
    properties (Constant)
        helpOption = '-h';
        
        configFileOption = '-c';
        
        resultPathOption = '-r';
        imagePathOption = '-i';
        imageFileNameRegexOption = '-iR'
        segmentationPathOption = '-s';
        segmentationFileNameRegexOption = '-sR'
        pxPerPlaneOption = '-pP'
        fluoTagsOption = '-fT'
        flatFieldFileNamesOption = '-fN'
        
        seedPrefixOption = '-p';
        maskPrefixOption = '-q';
    end
    
    methods
        
        
        function obj = CellXMaskCommandLineParser()
        end
        
        function r = hasResultPath(this)
            r = ~isempty(this.resultPath);
        end
                
        function r = parse(this, args)
            this.args=args;
            
            n = size(args,2);
            
            k = 1;
            while(k <= n)
                k = this.processArgument(k, n);
                k = k + 1;
            end
            if ~any(strcmp(this.args, '-h'))
                r = this.check();
            else
                r = 0;
            end
        end
    end
    
    
    methods (Access=private)
        
        
        function r = check(this)
            
            r = 0;
            if( this.execMode==1 )
                
                % Automatically create a CellX config if does not exist yet and
                % read that file. Can be modified, if needed to influence the
                % CellXMaskInterface outcome.
                if ~exist(this.configFile, 'file')
                    
                    c = CellXMaskConfiguration();
                    c.setMaximumCellLength(11)
                    c.setMembraneWidth(1)
                    c.setMembraneLocation(2)
                    c.setSeedRadiusLimit([4, 5])
                    c.membraneIntensityProfile = [1,8,10,8, 1];
                    c.setFluoAlignPixelMove(0) % assumed that segmentation mask
                    % and fluo channels are well aligned. Can be changed in the
                    % config file if needed
                    if isempty(this.configFile)
                        c.toXML('CellXMaskInterfaceCfg.xml')
                        this.configFile = 'CellXMaskInterfaceCfg.xml';
                    else
                        c.toXML(this.configFile)
                    end
                    
                end
                this.checkFileExists(this.configFile);
                
                if( isempty(this.configFile) )
                    err = MException('CellXMaskCommandLineParser:MissingCellXConfigurationFile', ...
                        'Missing CellX configuration (xml)');
                    r = [r, CellXMaskExceptionHandler.handleException(err)];
                end
                
                if( isempty(this.resultPath) )
                    err = MException('CellXMaskCommandLineParser:MissingResultPath', ...
                        'Missing result path (string)');
                    r = [r, CellXMaskExceptionHandler.handleException(err)];
                end
                
                if( isempty(this.imagePath) )
                    err = MException('CellXMaskCommandLineParser:MissingImagePath', ...
                        'Missing image path (string)');
                    r = [r, CellXMaskExceptionHandler.handleException(err)];
                end
                
                if( isempty(this.segmentationPath) )
                    err = MException('CellXMaskCommandLineParser:MissingSegmentationPath', ...
                        'Missing segmentation path (string)');
                    r = [r, CellXMaskExceptionHandler.handleException(err)];
                end
                
            end
        end
        
        function k = processArgument(this, k, n)
            
            arg = this.args{k};
            
            if( strcmp(arg, CellXMaskCommandLineParser.helpOption) )
                err = MException('CellXMaskCommandLineParser:UsageInvoked', ...
                    '');
                return
                
            elseif( strcmp(arg,  CellXMaskCommandLineParser.imagePathOption) )
                
                [value, k] = this.getNext(k, n, arg);
                this.imagePath = fullfile(value);
                
            elseif( strcmp(arg,  CellXMaskCommandLineParser.configFileOption) )
                
                [value, k] = this.getNext(k, n, arg);
                this.configFile = fullfile(value);
                
            elseif( strcmp(arg,  CellXMaskCommandLineParser.imageFileNameRegexOption) )
                
                [value, k] = this.getNext(k, n, arg);
                this.imageFileNameRegex = value;
                
            elseif( strcmp(arg,  CellXMaskCommandLineParser.segmentationPathOption) )
                
                [value, k] = this.getNext(k, n, arg);
                this.segmentationPath = fullfile(value);
                
            elseif( strcmp(arg,  CellXMaskCommandLineParser.segmentationFileNameRegexOption) )
                
                [value, k] = this.getNext(k, n, arg);
                this.segmentationFileNameRegex = value;
                
                
            elseif( strcmp(arg,  CellXMaskCommandLineParser.pxPerPlaneOption) )
                
                [value, k] = this.getNext(k, n, arg);
                this.pxPerPlane = value;
                
            elseif( strcmp(arg,  CellXMaskCommandLineParser.fluoTagsOption) )
                
                [value, k] = this.getNext(k, n, arg);
                res = textscan(value, '%s', 'Delimiter', ',');
                res = cellfun(@(x) strrep(x, '''', ''), res{1}, ...
                    'UniformOutput', false);
                this.fluoTags = res;
                
            elseif( strcmp(arg,  CellXMaskCommandLineParser.flatFieldFileNamesOption) )
                
                [value, k] = this.getNext(k, n, arg);
                this.flatFieldFileNames = value;
                
            elseif( strcmp(arg, CellXMaskCommandLineParser.resultPathOption) )
                
                [value,k] = this.getNext(k,n,arg);
                CellXMaskCommandLineParser.makeDirectory(fullfile(value));
                this.resultPath = fullfile(value);
                
            elseif( strcmp(arg, CellXMaskCommandLineParser.seedPrefixOption) )
                
                [value,k] = this.getNext(k,n,arg);
                this.seedPrefix = value;
                
            elseif( strcmp(arg, CellXMaskCommandLineParser.maskPrefixOption) )
                
                [value,k] = this.getNext(k,n,arg);
                this.maskPrefix = value;
                
            else
                err = MException('CellXMaskCommandLineParser:UnknownArg', ...
                    ['Unknown argument: ''' arg '''']);
                %throw(err);
            end
        end

        function [v,k] = getNext(this,k,n,arg)
            k = k+1;
            if( k>n )
                err = MException('CellXMaskCommandLineParser:MissingValue', ...
                    ['Missing value for option ''' arg '''']);
                throw(err);
            end
            v = this.args{k};
        end
        
    end
    
    
    methods(Static)
        
        function showUsage()
            c = CellXMaskConfiguration();
            fprintf('\n');
            fprintf('USAGE (linux,mac): CellXMaskinterface.sh PATH_TO_MCR OPTIONS\n');
            fprintf('USAGE (windows)  : CellXMaskinterface.exe OPTIONS\n');
            fprintf('\n');
            fprintf('This is CellXMaskInterface [version %d.%d.%02d], \na program for fluorescence quantification of microscopy images based on labeled segmentation masks.\n', ...
                c.major, c.minor, c.patch);
            fprintf('\nWritten by Andreas P. Cuny (2017-2022)\n');
            fprintf('\nBased on CellX');
            fprintf('\nWritten by S. Dimopoulos, C. Mayer\n');
            fprintf('Current Maintainer: Andreas P. Cuny (andreas.cuny@bsse.ethz.ch)\n');
            fprintf('\n');
                    
            fprintf('OPTIONS:\n');
            fprintf('   %3s %-20s  %s\n', CellXMaskCommandLineParser.helpOption,...
                '', 'Show this message');
            fprintf('\n');
            
            fprintf('REQUIRED OPTIONS: \n');
          
            fprintf('   %3s %-20s  %s\n', CellXMaskCommandLineParser.resultPathOption, ...
                'DST_DIR', 'Set the result directory (works only with the -si option, overwrites the result directory in the file series xml)');
            fprintf('\n');
            
            fprintf('   %3s %-20s  %s\n', CellXMaskCommandLineParser.imagePathOption, ...
                'IMAGE', 'The image used for control images (format: tif, (png, jpg, gif, bmp))');
            fprintf('   %3s %-20s  %s\n', CellXMaskCommandLineParser.imageFileNameRegexOption, ...
                'IMAGE REGEX', 'The image regex to identify the raw images (i.e. "BF*"))');
            fprintf('   %3s %-20s  %s\n', CellXMaskCommandLineParser.segmentationPathOption, ...
                'MASK', 'The segmentation mask (format: tif, (png, jpg, gif, bmp))');
            fprintf('   %3s %-20s  %s\n', CellXMaskCommandLineParser.segmentationFileNameRegexOption, ...
                'MASK REGEX', 'The segmentation mask regex to identify the mask files (i.e. "mask*")).');
            
            fprintf('\n');
            fprintf('MORE OPTIONS: \n');
            fprintf('   %3s %-20s  %s\n', CellXMaskCommandLineParser.configFileOption, ...
                'CONFIG', 'Set the path to a custom CellX config file. Otherwise a defaut will be created automatically.');
            fprintf('\n');
            fprintf('   %3s %-20s  %s\n', CellXMaskCommandLineParser.pxPerPlaneOption, ...
                'PX_PER_PLANE', 'Pixels per plane.');
            fprintf('   %3s %-20s  %s\n', CellXMaskCommandLineParser.fluoTagsOption, ...
                'IMAGE', 'Fluorescence image file name identifier (optional, format: tif, (png, jpg, gif, bmp)), option can appear multiple times, filename format: FLUOTYPE_.*');
            fprintf('   %3s %-20s  %s\n', CellXMaskCommandLineParser.flatFieldFileNamesOption, ...
                'IMAGE', 'Flat field image (optional, format: tif, (png, jpg, gif, bmp)), option can appear multiple times (order must correspond to fluorescence image order)');
            fprintf('   %3s %-20s  %s\n', CellXMaskCommandLineParser.seedPrefixOption, ...
                'OUT_IMAGE', 'Segmentation control image file name identifier. Defaults to seeding_*.');
            fprintf('   %3s %-20s  %s\n', CellXMaskCommandLineParser.maskPrefixOption, ...
                'OUT_File', 'Mask file name. Defaults to mask_*.');
            
            fprintf('\n');
            
        end
        
        function checkFileExists(file)
            if( 0==exist(file, 'file') )
                err = MException('CellXMaskCommandLineParser:FileNonexistent', ...
                    ['File ''' file ''' does not exist']);
                throw(err);
            end
        end
        
        function checkDirExists(file)
            if( 0==exist(file, 'dir') )
                err = MException('CellXMaskCommandLineParser:DirNonexistent', ...
                    ['Directory ''' file ''' does not exist']);
                throw(err);
            end
        end
        
        function makeDirectory(dir)
            if( 0==exist(dir, 'dir') )
                [s,msg,~] = mkdir(dir);
                if(s==0)
                    err = MException('CellXMaskCommandLineParser:DirCreationFailed', ...
                        ['Cannot create directory ''' dir '''(' msg ')']);
                    throw(err);
                end
            end
        end
        
        function value = makeBoolNum(value, arg)
            if( ~isnumeric(value) )
                value = str2num(value);
            end
            
            if( value~=0 && value ~=1 )
                err = MException('CellXMaskCommandLineParser:InvalidBoolValue', ...
                    ['Boolean value for option ' arg ' must be 0 or 1']);
                throw(err);
            end
            
        end
        
        function value = makeNum(value, arg)
            if( ~isnumeric(value) )
                [value,status] = str2num(value);
                if( ~status)
                    err = MException('CellXMaskCommandLineParser:InvalidIntegerValue', ...
                        ['Cannot convert value for argument ' arg ' to integer']);
                    throw(err);
                end
            end
        end

    end
end
