classdef CellXMaskExceptionHandler
    %CELLXMASKEXCEPTIONHANDLER Exception handler for the
    % CellXMaskInterface.
    
    properties
    end
    
    methods (Static)
        function ret = handleException(exc)       
            if( strcmp(exc.identifier, 'CellXMaskCommandLineParser:MissingCellXConfigurationFile') )
                fprintf(2, '\n\nERROR: %s.\n\n', exc.message);
                ret = 6;
            elseif( strcmp(exc.identifier, 'CellXMaskCommandLineParser:MissingImagePath') )
                fprintf(2, '\n\nERROR: %s.\n\n', exc.message);
                ret = 7;   
            elseif( strcmp(exc.identifier, 'CellXMaskCommandLineParser:MissingSegmentationPath') )
                fprintf(2, '\n\nERROR: %s.\n\n', exc.message);
                ret = 8;   
            elseif( strcmp(exc.identifier, 'CellXMaskCommandLineParser:MissingResultPath') )
                fprintf(2, '\n\nERROR: %s.\n\n', exc.message);
                ret = 12;
            elseif( strcmp(exc.identifier, 'CellXMaskCommandLineParser:UsageInvoked') )
                ret = 13;
            elseif( strcmp(exc.identifier, 'CellXMaskCommandLineParser:MissingValue') )
                fprintf(2, '\n\nERROR: %s.\n\n', exc.message);
                ret = 14;
            elseif( strcmp(exc.identifier, 'CellXMaskCommandLineParser:FileNonexistent') )
                fprintf(2, '\n\nERROR: %s.\n\n', exc.message);
                ret = 15;
            elseif( strcmp(exc.identifier, 'CellXMaskCommandLineParser:DirNonexistent') )
                fprintf(2, '\n\nERROR: %s.\n\n', exc.message);
                ret = 16;
            elseif( strcmp(exc.identifier, 'CellXMaskCommandLineParser:DirCreationFailed') )
                fprintf(2, '\n\nERROR: %s.\n\n', exc.message);
                ret = 17;
            elseif( strcmp(exc.identifier, 'CellXMaskCommandLineParser:UnknownMode') )
                fprintf(2, '\n\nERROR: %s.\n\n', exc.message);
                ret = 18;
            elseif( strcmp(exc.identifier, 'CellXMaskCommandLineParser:InvalidBoolValue') )
                fprintf(2, '\n\nERROR: %s.\n\n', exc.message);
                ret = 19;
            elseif( strcmp(exc.identifier, 'CellXMaskCommandLineParser:InvalidIntegerValue') )
                fprintf(2, '\n\nERROR: %s.\n\n', exc.message);
                ret = 20;
            elseif( strcmp(exc.identifier, 'SeedsChk:MaxNumOfSeedsExceeded') )
                fprintf(2, '\n\nERROR: Hough transform detected too many seeds (%s). \n 1) Check input image\n 2) Try modifying the min/max seed radius\n 3) Try increasing the seed detection cutoff.\n\n', exc.message);
                ret = 21;
            else
                fprintf(2, '\n\nERROR: %s\n\n', exc.message);
                for k=1:length(exc.stack)
                    fprintf(2, '   file: %s\n   name: %s\n   line: %d\n', ...
                        exc.stack(k).file,  exc.stack(k).name, exc.stack(k).line );
                end              
                ret = 22;
            end
            
        end
    end
    
end

