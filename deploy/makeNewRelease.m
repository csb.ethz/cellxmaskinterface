%
% CellXMaskInterface generate release script
%
% Requires: java, zip
%

% NOTE navigate and run from deploy subfolder
clear;
currentFolder = fullfile(pwd)

% path to CellXConfiguration class to get the current version number
mclassesAbsPath = cd(cd([currentFolder filesep '..' filesep 'mclasses']))
addpath(mclassesAbsPath);

%%
os = java.lang.System.getProperty('os.name');
os = os.replaceAll(' ', '_');

if( os.startsWith('Mac') )
    version = java.lang.System.getProperty('os.version');
    os = concat(os, version);
end

c = CellXMaskConfiguration();
releaseName = sprintf('%s_c%d.%02d', char(os), ...
    c.major, c.minor);


fprintf('Removing previous files ...\n');
try
    rmdir(releaseName, 's');
catch e
end

try
    delete([releaseName '.zip']);
catch e
end

fprintf('Building release %s ...\n', releaseName);

mkdir(releaseName);
if( os.startsWith('Mac' ) )
    dp = [matlabroot filesep 'bin' filesep 'deploytool -build batchCellXMaskInterfaceMac.prj'];
elseif( os.startsWith('Windows' ) )
    dp = ['"' matlabroot filesep 'bin' filesep 'deploytool.bat" -build batchCellXMaskInterfaceWin.prj'];
else
    dp = [matlabroot filesep 'bin' filesep 'deploytool -build batchCellXMaskInterface.prj'];
end
[e,m] = system(dp);

if( e==1 )    
    fprintf('%s\n', m);
    error('deploytool failed');
end

if( os.startsWith('Mac' ) )
   bindir = ['batchCellXMaskInterfaceMac' filesep 'distrib' filesep 'batchCellXMaskInterface*'];  
   copyfile(bindir, releaseName);
elseif( os.startsWith('Windows') )
   bindir = ['batchCellXMaskInterfaceWin' filesep 'distrib' filesep 'batchCellXMaskInterfaceWin.exe'];
   exe = [releaseName filesep 'batchCellXMaskInterface.exe'];
   copyfile(bindir, exe);
else
   bindir = ['batchCellMaskInterfaceX' filesep 'distrib' filesep 'batchCellXMaskInterface*'];
   copyfile(bindir, releaseName);
end

if( os.startsWith('Mac') )
    system(['cp CellXMaskInterface.sh.mac ' releaseName '/CellXMaskInterface.sh']);
elseif( os.startsWith('Windows') )
    % do nothing
else
    system(['cp CellXMaskInterface.sh.linux ' releaseName '/CellXMaskInterface.sh']);
end

if( os.startsWith('Mac'))
    mcr = '/Applications/MATLAB_R2012a.app/toolbox/compiler/deploy/maci64/MCRInstaller.zip';
elseif( os.startsWith('Windows' ) )
    mcr = 'C:\Program Files (x86)\MATLAB\R2011b\toolbox\compiler\deploy\win32\MCRInstaller.exe';
else
    mcr = '/net/bs-sw/sw-repo/bsse/MATLAB_R2011a/toolbox/compiler/deploy/glnxa64/MCRInstaller.bin';
end

% copyfile(mcr, releaseName);

if( ~os.startsWith('Windows') )
    system(['zip -r ' releaseName '.zip ' releaseName '/*']);
end

fprintf('Done built release %s successfully \n', releaseName);